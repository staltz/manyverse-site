const fs = require('fs')
const axios = require('axios').default

axios
  .get('https://gitlab.com/staltz/manyverse/-/raw/master/package.json')
  .then(function(response) {
    fs.writeFileSync(
      './static/latestversion.json',
      //JSON.stringify(response.data.version)
      // overriding to the last version that actually managed to build on all platforms
      // latest version will get a custom link on the download page for the platforms it managed to build for (probably just desktop linux)
      '"0.2310.9-beta"'
    )
  })
