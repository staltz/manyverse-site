---
path: '/blog/2022-05-update'
date: '2022-05-05'
title: 'May 2022 update'
author: 'André Staltz'
authorUrl: 'https://staltz.com'
---

Hi backers!

Thank you for your donation! There's lots of news to share with you this time. To start, here's a list of app improvements that Thomas and Andrew built for us last month, plus some guest contributions such as database tweaks from Arj03:

* 🎉  Feature: support searching for accounts too
* 🎉  Feature: option in Settings to toggle firewall
* ✅  Bug fix: SSB URI in a post becomes a mention
* ✅  (Desktop) Bug fix: app not working after canceling image attachment
* ✅  (Android) Bug fix: hide keyboard when leaving a private chat
* ✅  Bug fix: should not crash when recovering identity
* ✅  Bug fix: public tab "new" indicator incorrectly appearing
* ✅  Bug fix: unblocked messages were actually follows
* 🔷  Improve content loading performance a bit
* 🔷  Improve Dutch, Russian, German, Polish, Spanish translations

## Gatherings (work in progress)

Last month Andrew and Thomas also made progress on a bigger feature: gatherings, a way to host events and inform RSVP on SSB. I'll spare the juicy screenshots for later, when the feature is launched, but we've gotten far enough that we can render the basics of a gathering created on Patchwork. This will create a new way for people to interact through Manyverse, and make the app more of a Patchwork replacement. (Follow the [issue on GitLab](https://gitlab.com/staltz/manyverse/-/issues/1582) for more details.)

## Database compaction (complete)

My quest in supporting compaction in ssb-db2 is now complete! If you've followed this newsletter, you might remember that I've been focused on a side project: updating the database to allow it to "shrink" in size after some content has been deleted. I'm happy to inform that [compaction is now available in ssb-db2](https://github.com/ssb-ngi-pointer/ssb-db2/pull/339) version 4.1.0.

Next up, we need to design how to put this in Manyverse, and take extra precaution to make sure replication still works and that it doesn't corrupt your database indexes. Nicholas Frota (our UX and Product designer) has begun work on the design, and I'll be working on the implementation soon after the design is ready. The immediate utility of database compaction in Manyverse will be a new screen where you'll be able to manage storage taken up by peers and start a "defrag" process, which will behave pretty much like defragging PCs in the 1990s and 00s. Currently, when you block someone on Manyverse, their data still remains in your database, even though replication with them has halted. This is wrong, but we simply haven't had the tools to properly delete their data. Compaction will finally allow us to do that. It will also allow us to do more than that, when we support deleting older content from non-blocked peers.

Also, I must say I'm grateful for the SSB community who voted for this compaction project to receive a grant of 6000 USD! It's a database feature that should help all other apps that use ssb-db2.

## Roadmap redesign

Nicholas Frota is working on a lot of things. In a previous newsletter I mentioned how he's designing screens for exploring hashtags, and I just wrote above how he's working on the defrag screen. He's also redesigning our product roadmap. In the future, the website is going to get a new roadmap page where you can see the different features we're working on, and on what stage they're at. It will also include completed features, so you can see our history, and who sponsored our work. You should be proud that the vast majority of features was funded by Open Collective backers. The new website page will make that obvious.

![A screenshot of a design in Figma for the new roadmap website, displaying a header that says "Ongoing" and some "cards" below that for each feature being built](../../images/roadmap-v2.png)

## Private groups grant approved

I'm hyped to announce that we were approved for a [50k EUR grant from NGI Assure / NLnet](https://nlnet.nl/project/Manyverse-PrivateGroups/)! The goal is to bring private groups to production in Manyverse, and the team to make this happen is going to be me, Mix Irving, Arj03, Jacob, and Nicholas.

Our plan so far consists of: updating private chats in Manyverse to support unlimited members; designing and implementing "closed communities" where people can start threads with a limited number of members; designing safety mechanisms whereby some moderators can remove people from the group.

This is one of those things that are fundamental for Manyverse version 1. I remember attending an SSB meetup, *Extrasolar*, where we discussed what was needed in SSB before we can recommend it to the wider public. Cryptix answered the question with two words: "private groups", and it stayed stuck in my mind since then. SSB has a lively and lovely community, but communities also need boundaries, code of conduct, and safety. We've already seen some people leave due to the lack of privacy or boundaries. We want to address this need, and I think we have the perfect team to do it.

The project is going to start some time this month after we complete the paperwork with NLnet. I'll keep you informed as we go!

## DWeb Webinar talk

Finally, something nice that happened last month was a short talk I gave at a [webinar organized by DWeb (Internet Archive) and the Metropolitan New York Library Council](https://metro.org/decentralizedweb). I presented Manyverse in 10 minutes, how it's different, and why it's needed. Other co-speakers were Jay Graber (Founder of Bluesky PBLLC and former SSB community member) and Matthew Hodgson (Technical Co-founder of Matrix). During the 1 hour event, we also had a very interesting chat between us about the future of Twitter now that it's being bought by Elon Musk. The recording is coming out soon, so follow the [Internet Archive](https://twitter.com/internetarchive) to know when it's available.

![A screenshot of a Zoom video call with four persons shown: Wendy Hanamura from the Internet Archive, Matthew Hodgson, Jay Graber, and Andre Staltz](../../images/dweb-talk.jpeg)

Things are lining up for a lot more news next month, I hope you stick around to see what's coming!

Kind regards,

– @andrestaltz
