---
path: '/blog/2023-02-05'
date: '2023-02-05'
title: 'February 2023 update'
author: 'André Staltz'
authorUrl: 'https://staltz.com'
---

Dear backers,

![A photograph of staltz, arj, and powersource sitting on chairs around a desk, smiling](../../images/p2p-basel-meetup.jpg)

In the picture above, you'll see my dear coworkers `arj` and `powersource`, who have been lately focused on SSB private groups. Coming back from [p2p-basel](https://p2p-basel.org/), my thoughts are spinning about the future of SSB, and Manyverse. Blending with researchers in the University of Basel, we spoke about the big-picture organizational issues as well as the low-level cryptographic and distributed systems issues. We came to conclusions (like how to cryptographically remove members from private groups), but we also opened up new discussions (like what we think about the SSB [Consortium](https://opencollective.com/secure-scuttlebutt-consortium) and how to structure it in the future). It was also a place to be inspired, seeing how the [Computer Networks Group](https://cn.dmi.unibas.ch/) is transmitting compressed SSB messages over LoRa in an urban setting, and how [Earthstar](https://github.com/earthstar-project), [Cabal](https://github.com/cabal-club/cable/), and [p2panda](https://p2panda.org/) are maturing.

I also gave a talk about the near-future developments in Manyverse. Below you'll see the main slide from that presentation, it is displaying the major past, current, and future projects affecting Manyverse.

![Diagram of a roadmap with different interdependent projects linked to each other](../../images/p2p-basel-roadmap.png)

This roadmap honestly scares me. It's a lot of work to do, and if we don't do it, eventually SSB apps will all suffer from out-of-memory crashes and stop working. It's a critical time, where we have to upgrade or die. I may sound like I'm exaggerating, but I'm just thinking about this way ahead of time, before it's a reality. This gives us time to properly plan and work on it. Sharing the roadmap was good, because people got to chip in on the solutions. I already learned a thing or two from that feedback.

## What's new in Manyverse

Back home, I've been carefully tracking ex-Patchwork users wishes regarding Manyverse desktop, and working on delivering them one by one. This is a long list of microfeatures that make your life easier when using the app, and I hope you like it! Three of these are brought to you by Andrew Chou:

* 🎉  Feature: button on the Profile to open private chat
* 🎉  Feature: message timestamps can be clicked
* 🎉  Feature: show followers in common on the profile
* 🎉  (Desktop) Feature: paste or drag-and-drop images
* 🎉  (Desktop) Feature: warn of too large blobs attached
* ✅  Bug fix: prevent posting when message is too long
* ✅  Bug fix: subscribed status in hashtag search
* 🔷  Clicks on Thread previews pass through to clickables
* 🔷  Improve Brazilian Portuguese and Russian translations
* 🔷  Improve Resync screen with a cancel button
* 🔷  Change how About and Thanks look like
* 🔷  Improve Profile screen followers counters
* 🔷  Improve style of toggle button in dark mode
* 🔷  Remove "occupies" counter on the Profile screen

## Google Play Store

We got some bad news last month: Google Play Store rejected our app update, because we seemingly lack a "Terms of Service" when users start using Manyverse. We appealed, explaining to them that Manyverse is local-first, and there is literally no "Service" involved. The answer came back: still rejected (without much detailed feedback on why).

But hope is not lost. I'm in contact with a Terms of Service specialist, and we're trying to come up with a Terms of Use document that makes it clear that we are not a platform nor a service, but also in a way that Google Play Store will accept. I'll keep you posted. I don't think this is the end of Manyverse on the Play Store, it's just a very annoying obstacle that unjustly consumes our time and energies.

Such things happen when you're creating something radically new: it doesn't fit within the existing standards and assumptions, and you have to stretch or break those standards.

– @andrestaltz
