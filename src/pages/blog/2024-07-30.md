---
path: '/blog/2024-07-30'
date: '2024-07-30'
title: 'Private groups - final blogpost'
author: 'Jacob Karlsson'
authorUrl: 'https://sunbeam.city/@powersource'
---

Hi! I'm writing this blogpost to communicate what happened with the [Private groups grant](https://nlnet.nl/project/Manyverse-PrivateGroups/) we were working on. In here I'll try to summarize work we did since the [last private groups update](https://www.manyver.se/blog/2022-11-25) (don't forget the [user experience research](https://www.manyver.se/blog/2023-05-19) that was done too) and until approximately summer last year (sorry for the late update!).

We researched and wrote down the [ssb group exclusion spec](https://github.com/ssbc/ssb-group-exclusion-spec). It's been very well thought through and is well written, I can recommend at least skimming it if you're into this kind of stuff. Notable things include
* Key rotation on member exclusion: when excluding (removing) a member of a group, we don't just politely ask them to leave. Under the hood we create a new group with a new encryption key that we invite everyone to except for the excluded member.
* New metafeeds on key rotation: when doing this member exclusion key rotation, everyone in the group starts posting on new feeds (see [ssb-meta-feeds-group-spec](https://github.com/ssbc/ssb-meta-feeds-group-spec) for meta feed details). This means that on top of the e2e encryption of the group, the excluded member won't even see the new activity in the group, unlike how things work in the traditional ssb-box encryption where everything is posted to the same feed.
* Efficient conflict resolution: instead of spinning up a blockchain (lol) in case of conflicting states, we assume some minimal level of trust and use the tried-and-true highly efficient [Array.sort()](https://github.com/ssbc/ssb-tribes2/blob/f38e1ac8a4570337746e862f35dc373999dff292/lib/epochs/index.js#L484) ([spec](https://github.com/ssbc/ssb-group-exclusion-spec#43-tie-breaking-rule)). 

We implemented this spec into the [ssb-tribes2](https://github.com/ssbc/ssb-tribes2) module. We also did a bunch of work to make it more resilient to crashes by being able to recover from the middle of an exclusion that was aborted.

We also developed [ssb-tribes2-demo](https://github.com/ssbc/ssb-tribes2-demo/) to demonstrate the ssb-tribes2 module in action. It's a simple group chat app where you can invite and kick people, message each other with end to end encryption and without any servers or internet involved.

One thing we're releasing now is [a small patch release](https://gitlab.com/staltz/manyverse/-/merge_requests/391) (no new features nor big bug fixes) of Manyverse to get to use some newer versions of code modules released during the private groups development. Automatic updates should be rolling out over the next few days. NOTE though that Manyverse won't actually gain Private groups support, unfortunately (Manyverse also won't receive more updates in general, as we've [announced before](https://www.manyver.se/blog/2024-07-03)). That was the original plan but due to time and budget constraints that was unfortunately not possible. Hopefully these specs and modules can live on and inspire other efforts. 