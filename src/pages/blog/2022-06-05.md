---
path: '/blog/2022-06-update'
date: '2022-06-05'
title: 'June 2022 update'
author: 'André Staltz'
authorUrl: 'https://staltz.com'
---

Hey backers,

This is the 46th newsletter I'm writing, and soon Manyverse will be a 4 year old project. That's a long time for a project that still isn't "ready". But the work we're doing is actually insanely hard: to make peer-to-peer social networking as user friendly and seamless as centralized platforms, while financially under-resourced, and working within the scientifical limits of cryptography and distributed permissionless systems. On the other hand, I am no where close to quitting, and — as you'll read in this newsletter — this year is a turning point for SSB and Manyverse.

First, let's look at the crown jewel of this update: **support for Gatherings** shipped! Version 0.2206.3 has:

* 🎉  Feature: attend Gatherings
* ✅  (iOS) Bug fix: crash on iPad when attaching images
* ✅  (iOS) Bug fix: dont show Bluetooth sync choice
* ✅  Bug fix: crash on malformed contact msgs
* ✅  Bug fix: emoji picker should show your favorites
* 🔷  Improve Russian translation

![A graphic design where there is a phone on the right displaying the Manyverse app showing a Gathering posted by Zenna named "Solpunk 2022"; Text to the left of the graphic design says "Attend Gatherings"](../../images/gatherings-attend.png)

Andrew Chou and Thomas Belin had a wonderful collaboration building the new Gatherings feature for you over the past 4 weeks. The new Gatherings feature means that you'll be able to see "events" created in Patchwork, details like date and time, and the ability to confirm attendance. Previously, all Manyverse users couldn't see these gatherings rendered in the app, which meant that you may have missed some nice opportunities to meet people. But no more! If you're in Europe, go take a look at Zelf's "Solpunk '22" gathering, which is happening late August, so there is still time to consider it and RSVP!

So far, Gatherings are read-only, which means that you can't create events, but you can see events created by others and attend. We're going to work on a follow-up feature to allow creating them too. It's not so easy to build because it involves *date pickers*, which are surprisingly full of corner cases. We want to build it properly, not rushed, y'all deserve it.

## Starchart

Let's talk big picture for a moment. Some of you may recall the [newsletter update I sent out in the beginning of this year](https://www.manyver.se/blog/2022-02-update), where I wrote about "sustainability, onboarding, and safety". These are three big themes that we're working towards this year. They are not just improvements to SSB / Manyverse, they are fundamental fixes that have to be done to "rescue" SSB. I'm calling them S.O.S., not only because that's the acronym for Sustainability, Onboarding, Safety, but also because of the real urgency implied.

SSB as it currently works has difficulties with scalability, and I'm not talking about growing to millions of users, I'm talking about growing *one* user's account over the course of years. Everyone hits a storage limit sooner or later. 1GB is already too much storage taken up, for most people. So "sustainability" is about keep storage within limits. SSB also has difficulties with getting people on board, "onboarding". You know how hard it is to get invites, wait for initial sync, and so forth. Finally, a lot of people interested in SSB are concerned joining if deletes are not yet available, or before community boundaries and privacy are ensured. Thus "safety".

We're already addressing all of these 3 themes. For Sustainability, [I finished the database compaction project](https://www.manyver.se/blog/2022-04-update), and our product designer Nicholas is now sketching a new "Storage management" screen for Manyverse. For Onboarding, we're sketching a protocol for [the future of room invites](https://staltz.github.io/ssb-room-broker-auth-spec/), and Nicholas is [designing the flow for those invites](https://www.manyver.se/epic/1887). For Safety, we have a whole team working on [bringing Private Groups to Manyverse](https://www.manyver.se/blog/2022-05-update).

I compiled these themes and other future themes in a document I'm calling our ⭐ [Starchart](https://gitlab.com/staltz/manyverse/-/wikis/Starchart/). Unlike the roadmap, it's not a list of features to build, it's a list of themes to be mindful of, and to give priority. The Starchart informs what is priority in the roadmap.

## New roadmap page

Speaking of the roadmap, we're happy to present you the new roadmap page on our website: ✨ [manyver.se/roadmap](https://www.manyver.se/roadmap/)! Nicholas and I worked on this last month and the idea is to more clearly show you what we're working on *now*, something that the previous roadmap page didn't do at all. We also have a page for each epic we're working on, giving details.

More broadly, we want people who support us — you! — to know how responsible and dedicated we are. The new roadmap page now also shows the features we completed and delivered. Without that, website visitors wouldn't know exactly how much we've done. So when we promise we'll do more, we can actually achieve those.

Managing and prioritizing the order of things to do has been very challenging to me. There seems to be dependencies everywhere. For private groups, we will need invites built first. For invites, we will need storage management built first. For groups, we will need metafeeds and partial replication. It feels like everything needs to happen all at once. And we currently have three different sources of funding: OpenCollective, NGI Assure (NLnet), and SSB community grants.

To manage all this, I made a chart to keep myself sane and plan what needs to be done first, by whom, and funded from where.

![Mermaid JS diagram of tasks to be done, their interdependency, and in which team they belong to](../../images/2022-ssb-situation-map.png)

## Private groups team

The Batts (that's what we're calling our grant project team, a portmanteau of "Bat" with "Butt") have been busy starting work on private groups. We have weekly meetings, and so far our job consists of design scoping and making new technical protocols. Here's our happy faces in a video call:

![A screenshot of a Jitsi video call with 5 participants: Mix, Anders, Andre, Jacob, Nicholas](../../images/batts-team-jitsi.jpg)

Design is a big part of the project, and it has to come first, so Nicholas has been busy defining the scope and mapping the assumptions for "Groups", the name we're giving for private groups in Manyverse. We compiled a list of personas and assumptions [here in this GitLab issue](https://gitlab.com/staltz/manyverse/-/issues/1904). As for technical protocols, we're drafting how should Groups be stored and replicated, and it'll be via metafeeds and partial replication. This also means that we will have to put metafeeds and partial replication in production in Manyverse sometime soon.

So many things are happening this year! I hope this big list of things to do untangles itself and soon you'll see in practice how fundamental these changes are going to be to SSB. Root for us.

Kind regards,

– @andrestaltz
