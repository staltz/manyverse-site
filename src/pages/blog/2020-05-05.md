---
path: '/blog/2020-05-update'
date: '2020-05-05'
title: 'May 2020 update'
author: 'André Staltz'
authorUrl: 'https://staltz.com'
---

Hey backers!

I hope all of you are doing well. April was a good month for Manyverse, we got now 106 backers, and the app loads and runs faster. I worked 84 hours and you funded 41 hours (this number has only *increased* every month for the past half year!) of those. Here's what we're releasing this time:

* 🔥 Improve app startup time (update node.js mobile) ([v0.2005.5](https://gitlab.com/staltz/manyverse/commit/4ac4fb0ceacb6c5e12d20695d80512b5cbf8e713))
* 🔥 Improve UI performance of the connections screen ([v0.2005.5](https://gitlab.com/staltz/manyverse/commit/30e7400a4536d5c071c03b9872921073a33e627d))
* 🔥 Improve UI performance of the three main tabs ([v0.2005.5](https://gitlab.com/staltz/manyverse/commit/d8ead4f60fb59f99488ce7daf9581cb960d4be61))
* 🔥 Speed up building of database indexes a bit ([v0.2005.5](https://gitlab.com/staltz/manyverse/commit/c201feeb7742e849a2f50824123c38e9bec4b733))
* ✅ Bug fix: don't show duplicate connections ([v0.2005.5](https://gitlab.com/staltz/manyverse/commit/6a22e2580015fe958762132961b8d5350844608d))
* ✅ (iOS) Bug fix: back button logic on some screens ([v0.2005.5](https://gitlab.com/staltz/manyverse/commit/6790b707daa474119f555b4576a5638385827946))

This month there aren't new features, because a lot of effort was put on performance work. But the results are massive: **the app now loads up significantly quicker than before**. For instance, Manyverse on my phone used to take 16 seconds to fully load, now it takes only 7 seconds! The connections screen is also significantly less glitchy too. That said, we still have many more ideas to implement before we can say the app is fast overall. For instance, we should soon begin tackling the challenge of making the initial data sync efficient, see issue [#745](https://gitlab.com/staltz/manyverse/issues/745). But, for now, update the app and check how the speed boost feels!

## Localization (soon)

Another good news is that language **localization** in the app is almost ready. It is basically done (issue [#362](https://gitlab.com/staltz/manyverse/issues/362)), but before releasing the feature I still need to gather some translations to other languages. Very soon I'll be contacting the people who volunteered to translate, and they'll begin replacing the texts. I too will contribute with translations for one language. If you also want to volunteer to translate, please contact me at [contact@staltz.com](mailto:contact@staltz.com), I'll publish translation instructions in the next weeks. We would like to use the [Weblate](https://weblate.org) platform for that, but we still don't have a FOSS account over there, so in the meantime, translation will happen as [pull requests](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) for JSON files.

## Feed redesign (soon)

On the previous newsletter update, I introduced David Gómez to you all, and he has been doing some great UI/UX contributions to the app. Recently, David and I have been **redesigning the look and feel of threads** on the Public board screen (issue [#843](https://gitlab.com/staltz/manyverse/issues/843)). It will look much cleaner, and easier to make sense of the *fire hose* of incoming content. Additionally, the redesign should have some performance benefits as well, due to the thread cards having the same predictable height. We're quite close to finishing the UI design, so I hope that for the next newsletter we'll implement it and ship it.

Let's stay in touch!

— @andrestaltz