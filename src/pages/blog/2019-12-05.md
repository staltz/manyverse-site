---
path: '/blog/2019-12-update'
date: '2019-12-05'
title: 'Dec 2019 update'
author: 'André Staltz'
authorUrl: 'https://staltz.com'
---

What's up backers! This is the last newsletter of the year 2019, I hope you had a wonderful year. And I'm looking forward to 2020. Last month, I worked on Manyverse for 64 hours, of which 23 hours were funded by you, a total of 79 backers. In November we made a couple of releases:

- **Five bug fixes and improvements** [v0.1911.19](https://gitlab.com/staltz/manyverse/blob/master/CHANGELOG.md#0191119-beta)
- **Feature: mention other accounts using @** [v0.1911.27](https://gitlab.com/staltz/manyverse/blob/master/CHANGELOG.md#0191127-beta)

In version 0.1911.19 we got a couple of crashes fixed, thanks to your and other users' reports. And Jacob (*Powersource*) contributed to fix follow messages so that they say "unfollowed" or "unblocked" instead of the more confusing "discharged".

![A graphic design where there is a phone on the right, displaying the Manyverse app open, and text on the left; The text says "Mention others using @ to open a list", and the phone has Manyverse open with the Compose Message screen showing a text field with the contents "@r" and a list with two accounts that have names starting with the letter R](../../images/mention-others.png)

**The new @ mention** is a feature I myself have been wanting for quite some time. When composing a message, when you type "@" it shows a list of possible accounts you want to notify. On apps like Patchwork, this will give those accounts a notification. We will soon get to implement that too in Manyverse, but first we had to have this @ mentioning feature. I hope you like it too!

In November I was traveling a lot, that's why I had less time for developing Manyverse. But I had some incredible encounters. In the previous newsletter I told you about the community networks event in Brazil ([check it out here](https://www.manyver.se/blog/2019-11-update) in case you missed it), but soon after that I traveled to Netherlands for a workshop and conference, *Frontmania*. My conference talk was actually about [Node.js for Mobile](https://code.janeasystems.com/nodejs-mobile), an essential piece of technology in Manyverse. If you'd like to learn more how Manyverse works, the talk might interest you! The video hasn't been published yet, but follow [Frontmania's YouTube channel](https://www.youtube.com/channel/UCxoPrvkFetPN5cFHZ_Vd6RQ/videos), I believe the video will be out in a few weeks.

![On a large stage decorated with bamboos and plants, Andre Staltz is looking at this computer while he gives a technical talk about programming; behind and above him you can see a large projected screen where slides are shown](../../images/frontmania19-talk.jpeg)

Then there was a trip to Malmö, Sweden where seven **Scuttlebutt community members and developers from Europe** gathered to plan a consortium to receive EU funding for Scuttlebutt. Our desire is to join forces and apply for grants together, to increase our chances. We just got started with this, so there isn't much to report, but it's promising. Of course we also had a great time together, since Scuttlebutt gatherings aren't that common. In the picture below we're in a brainstorming session.

![On a winter day in a living room with couches and a table in the middle, four persons are taking a selfie showing the table with a large paper, pens, post-its, snacks, coffee mugs; people are smiling and some of them have their hands displaying two fingers in V shape](../../images/eu-consortium-meeting.jpeg)

Since then, I've been **back home working on Manyverse**. I'm currently focused on something too exciting, that it's better to keep as a secret for now. As a teaser, it includes some UI and brand redesign, but it's much more. I can't wait to show you all once it's done!

By the way, in November we also got a [Wikipedia article on Scuttlebutt](https://en.wikipedia.org/wiki/Secure_Scuttlebutt) finally approved (after months)! I helped kick-start it, and others also chipped in some edits.

A Merry Christmas and see you again in 2020!

— @andrestaltz