---
path: '/blog/2022-03-update'
date: '2022-03-05'
title: 'March 2022 update'
author: 'André Staltz'
authorUrl: 'https://staltz.com'
---

Hey backers,

Last month [I asked on Twitter](https://twitter.com/andrestaltz/status/1491765322881376256) for people to donate to Manyverse, and to my surprise, we got +$500 (recurring) to our budget. Thank you! And welcome to the new backers reading this newsletter.

I worked last month on one big new feature and a bunch of improvements and bug fixes. Version **0.2203.3-beta** introduces:

* 🎉  **Feature: Resync screen guides you after recovery**
* 🎉  Feature: reply counter opens thread at the bottom
* 🎉  Feature: allow starting a private chat with yourself
* 🎉  Feature: allow linking to misc URIs like dat:// and cabal://
* 🎉  Feature: copy SSB URI of messages and feeds
* ✅  **(iOS) Bug fix: crashing when returning from lock screen**
* ✅  (iOS) Bug fix: Wi-Fi discovery of peers
* ✅  (Desktop) Bug fix: clickable regions in Reactions screen
* ✅  (Desktop) Bug fix: make markdown cypherlinks focusable
* 🔷  (Desktop) Improve style of focused UI elements
* 🔷  (Desktop) Improve layout of the quick emoji modal
* ✅  Bug fix: dont crash after running for many hours
* ✅  Bug fix: faded threads should not allow clickthrough links
* ✅  Bug fix: when thread is missing, show explanation
* 🔷  Improve Spanish translation

The new **Resync** screen is an additional screen after you press "Recover identity" on the Welcome screen. Our goal with this new feature was to help you restore your data backed up with other peers, in a way that is safe against "forks" (which irrecoverably break your account). The Resync screen guides you on what to do to restore your data, suggesting to connect to a peer over LAN or with a server invite, and displaying download progress.

The other noticeable feature is that clicking on the *reply counter* will now **open the thread and scroll to the bottom**. This was especially important on long threads that previously demanded you to scroll manually for a long time, which is even harder on mobile where you don't have the Home / End buttons that desktops have. We realize that threads need better user experience when navigating and creating subthreads, so this is an area we will be working on a lot in the next months. We have many ideas to improve it.

Apart from those two, I'd like to highlight one bug fix that was a true monster to debug. It's been happening for years. Manyverse **iOS** no longer crashes when you unlock your device and Manyverse was the most recent app. For those of you who are curious about the technical details, the culprit was that iOS closes UDP sockets when you lock the device, and we used those for LAN syncing, so the solution was just a matter of [handling UDP socket close errors](https://github.com/staltz/ssb-lan/commit/460059008ac4e693863601ce60e133fef2d5ea7a) and resetting LAN sync.

**Boosting our team, welcome Thomas and Andrew!**

As I hinted in previous monthly update, I felt the need to hire a second person to work on features and fixes. I put out a [job offer](https://www.manyver.se/blog/2022-02-hiring) on our blog, and within a few days, we found 2 candidates that were perfect. They both only had Fridays available, so it made sense to hire both, as their combined dedication would mean 60+ hours per month, and we can collaborate as a team of 3 developers every Friday.

Say hello to [Andrew Chou](https://github.com/achou11) and [Thomas Belin](https://github.com/atomrc)! Andrew works at [Digital Democracy](https://www.digital-democracy.org/) on [Mapeo](https://www.digital-democracy.org/mapeo/), a peer-to-peer mapping app for indigenous communities (which I also briefly worked on), and is thoroughly familiar with the tools we use in Manyverse: nodejs-mobile, Electron, React Native. Thomas works at [Wire](https://wire.com/) and is a Cycle.js expert, great at using various tools and digging deep into the stack, and we've met a couple times in Cycle.js conferences too!

Both of them are great at communicating over GitHub/GitLab, and enthusiastic to work with Manyverse, so I'm absolutely confident they will boost our project! And it's going to be fun working with them too. Thomas is starting mid-March, and Andrew started this week, already fixing two bugs!

This is going to happen for a limited time, over 2 or 3 months, and they will be paid with the funds you donate to our Open Collective, while I'm going to work mostly unpaid (using my savings) on doing some "surgical changes" to SSB. We're overdue with putting [partial replication](https://github.com/ssb-ngi-pointer/ssb-secure-partial-replication-spec) into production, because we can't keep the database growing indefinitely (it makes *everything else* harder). Last year [arj](https://github.com/arj03/), [glyph](https://github.com/mycognosist) and I worked on the JavaScript implementation of partial replication under the [NGI Pointer project](https://www.manyver.se/blog/2020-10-update), but the project is over, and we still need to continue on what we started. I intend to work a lot on the [database](https://github.com/ssb-ngi-pointer/ssb-db2), [compactions for deleted records](https://github.com/ssb-ngi-pointer/ssb-db2/issues/306), [testing and improving index feeds in Manyverse](https://github.com/ssb-ngi-pointer/manyverse-with-index-feeds). I'll lean on arj's and Dominic Tarr's support and hopefully bring back some good news!

Have a great weekend, and... Glory to Ukraine 🇺🇦🙏

Cheers,

— @andrestaltz
