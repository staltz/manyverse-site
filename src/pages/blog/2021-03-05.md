---
path: '/blog/2021-03-update'
date: '2021-03-05'
title: 'March 2021 update'
author: 'André Staltz'
authorUrl: 'https://staltz.com'
---

Hey backers!

I come with great news, today there's a release of Manyverse with the long-awaited new database! Minutes before writing this I was doing final tweaks and measurements to make sure that the app stays quick and responsive in all sorts of circumstances (is it migrating? is it doing initial sync? is it fetching data? is it mixing all of these?). But now is time to ship. For the past two weeks I've been using Manyverse with this new database on my own phone and it's working all fine, this can be shipped!

## New release

**Version 0.2103.5** is rolling out on Google Play, App Store, and for cutting-edge Android users, the APK is available at [manyver.se/apk](https://manyver.se/apk). We submitted an update to F-Droid, and did our best, let's hope it works on their servers.

![A graphic design where there is an emoji of a lightning bolt and some text that says "3x faster; Quicker initial sync; Detailed progress bars"](../../images/ssb-db2.png)

* 🎉 New database promises faster performance
* ✅ Bug fix: improve text contrast for code blocks (by David Gomez)
* 🔷 Progress bars indicating work by the new database
* 🔷 New translation: Turkish at 79%

Changing the database was like swapping the engine of a car from combustion to electric, it was very hard to keep everything working flawlessly and maintaining all features. After many months of intense work, sometimes getting really stressed out or downhearted about it, all the features work. Special thanks given to Anders ([arj03](https://github.com/arj03/)) (co-authored the new database with me) who several times helped me out with Manyverse-specific issues in getting this working. I think we had over 10 pair programming sessions.

There may be some hiccups here and there, maybe even some new crashes (although not that frequently), but these are things we expect to fix in the coming weeks. We also have more ideas up our sleeves how to improve the performance even further (for example, with the user name autocomplete feature). And now that this new database is in, we can finally begin to confidently write new database queries for new screens and new information, such as notifications of your mentions, follower count, person search, etc. In many ways, this is a new beginning. Some hours from now, I'll get on a video call today with the [NGI Pointer team](https://www.manyver.se/blog/2020-10-update) to celebrate exactly this!

## UX Project complete

The work Wouter Moraal has been doing to uncover the best design for onboarding new users to Manyverse is now complete! You can see all the documents about "Welcome to the Manyverse" at [manyver.se/ux-research](https://www.manyver.se/ux-research/), where we describe how users expect the app to guide them to invite other users, and how new users want to experience their first minutes and interactions on the app.

Among several other conclusions and recommendations of new screens, one of the outcomes is the design of "unique usernames" called *aliases* on room servers, and how all of this will help people find each other and connect over rooms.

## Rooms 2.0 in construction

The NGI Pointer team is tasked with, among other things, designing and implementing the next edition of [room servers](https://www.manyver.se/blog/announcing-ssb-rooms). You can read the [design document here](https://ssb-ngi-pointer.github.io/rooms2/). Guided by community feedback and Wouter's *Welcome to the Manyverse*, we now know what to do, and we have begun implementing these new servers!

[Written in Go](https://github.com/ssb-ngi-pointer/go-ssb-room), led by [cryptix](https://github.com/cryptix) (co-author of go-ssb), the new server codebase has a web interface (like the [existing ssb-room](https://github.com/staltz/ssb-room) has too), with admin log-in (unlike ssb-room), member management, invite management, localization, sign-in with SSB, etc. Building on top of the successful adoption of ssb-room, go-ssb-room will be a big deal for the SSB community, improving onboarding, community privacy, shared responsibilities, and more. I've been doing my first steps in Go by building go-ssb-room, and so has [cblgh](https://github.com/cblgh), a recently-joined member of the NGI Pointer team. You may know cblgh from their fantastic work on [Cabal](https://github.com/cabal-club/) and [Trustnet](https://cblgh.org/articles/trustnet.html). The stage is set for awesome outcomes.

In Manyverse, we'll aim at bringing Room 2.0 features as soon as possible, such as aliases and sign-in with SSB. Me being in both NGI Pointer and Manyverse helps make sure that the new designs are put into production as soon as they are stable.

I can't wait to show you all these features! 2021 is a busy year but a very exciting one.

Best wishes,

— @andrestaltz