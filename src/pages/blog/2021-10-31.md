---
path: '/blog/2021-10-interview-appoftheday'
date: '2021-10-31'
title: 'Featured in DownloadAstro.com'
author: 'André Staltz'
authorUrl: 'https://staltz.com'
---

DownloadAstro.com is an online catalog of software, and they picked Manyverse as their *App of the day*. Below is a transcript of the interview they conducted with Andre Staltz, also on their website: https://appoftheday.downloadastro.com/app/manyverse/

---------

**Did you have any prior development or coding experience?**

I have professional experience with deploying mobile apps to Google Play since 2013 and have been coding since 2005.

**What was the most challenging aspect of developing mobile app?**

Manyverse is a very unique app for two reasons: it does peer-to-peer communication with other phones nearby, and it uses Node.js for that. Putting Node.js into a mobile app is very unusual and comes with its own set of challenges and there are very few people on the internet who can help you. We had to use Node.js because it was the simplest way of using the existing "SSB" libraries, which comprise the peer-to-peer social network protocol that we use. Being peer-to-peer also means that the app has to act as a server (and simultaneously client, too) and this has additional difficulties on mobile because the operating system (especially iOS) frequently pauses apps that are not in the foreground.

> Andre's development experience spans nearly two decades, and in all these years, he believes in transparency and digital freedom.

**Name a few of your favorite apps and reason you love them.**

I like **Telegram** because it has a superb user experience and UI performance is always smooth. It's also free of ads and any such distractions, and it's open-source which means you can learn a lot about how they build some features. Another honorable mention goes to [**Tuner**](https://github.com/billthefarmer/tuner) because it's a one-screen app with no buttons, and does what it promises really well. It always opens up instantly and immediately provides value, with good UI performance and technical accuracy for musicians.

**How long have you been working on this app?**

I started working on Manyverse early 2017, and published the first version on September 2018. Every month since then I have written a monthly newsletter informing people of updates to the project.

**What need of the user did you have in mind when developing this app?**

People need some form of social networking on mobile that does not involve a company acting as an intermediary. Manyverse is built to respect users' privacy, respect their ownership of data, and does not have any ads or analytics or tracking of any kind. We just want to build a good app that solves communication needs, without monetizing the features in the app. We are also open-source and transparent.

**In what way do you think your app is better than similar apps on the market? Please describe in detail what innovation you think you bring and what you are proud of in your app.**

All social content is stored on the user's device, so this means you can always use it, whether online or offline in the airplane or in the jungle. I believe Manyverse is the only social networking app that provides this guarantee, and it means it's very resilient, it won't go "down", it won't disappear suddenly, and it doesn't depend on some company existing in order for it to work. Another benefit of this design is that it respects your privacy very well. On the App Store, I see that all other social apps collect user data of some kind, but Manyverse's privacy section on the App Store is completely blank, we don't collect any data at all.

**What are your future plans and expected features of the coming new versions of this app?**

We are currently working on developing Manyverse for desktop (macOS, Windows, Linux), and it's almost ready. On [our roadmap](https://www.manyver.se/roadmap/), we describe all the future features, such as better community safety systems, and better usage of the device storage.

**Assuming new users of your app are reading this page. What do you want to ask them to do (contact you about X, Share the app, etc.)?**

We have no monetization, but I'm working full-time on this app, so the best you can do is financially support us. Even $2 monthly makes a difference. On top of that, it means you also subscribe to our monthly email newsletter where we update you on the progress we made. [Our donate page](https://www.manyver.se/donate/) has all the details on how we use the funds received, we are committed to radical transparency.
