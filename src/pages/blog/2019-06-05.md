---
path: '/blog/2019-06-update'
date: '2019-06-05'
title: 'Jun 2019 update'
author: 'André Staltz'
authorUrl: 'https://staltz.com'
---

Hey backers! Thank you for being part of this movement, your continued support is vital for a steady progress. With **64 backers**, we are now as big as some other medium-sized projects on Open Collective such as [Electron](https://opencollective.com/electron), [Jekyll](https://opencollective.com/jekyll), [Rollup](https://opencollective.com/rollup), and [Beaker Browser](https://opencollective.com/beaker). May was a month of many conferences for me to attend, and there is still one more conference left (tomorrow) before I can go back to normal development. This has affected the amount of work I could put into Manyverse, as I worked only 40h last month. Those hours were used to develop:

- **Bug fixes** [v0.1905.10](https://gitlab.com/staltz/manyverse/blob/master/CHANGELOG.md#0190510-beta)
- **Improvements to the crash report system** [v0.1905.09](https://gitlab.com/staltz/manyverse/blob/master/CHANGELOG.md#019059-beta)
- Collaboration with other SSB mobile app developers, [#393](https://gitlab.com/staltz/manyverse/issues/393) and [#398](https://gitlab.com/staltz/manyverse/issues/398)

Manyverse is so far the only mobile app for SSB networks, but I don't want this to be the case always. It will be better for the whole ecosystem if there is choice and variety. [Luandro](https://github.com/luandro) from the SSB community is working on another mobile app, that initially will be a fork of Manyverse tailored for indigenous communities in Brazil. In May we had a few audio calls to coordinate **how Manyverse would interoperate with other SSB apps on Android**. Currently, Manyverse has no way of exporting your account or using it outside this app. Luandro and I found a good (and simple to implement) architecture for sharing and exporting an account, and this is a fundamental step in allowing multiple SSB apps on Android.

May was a month of coordination, meeting lots of interesting people, and communicating in general. Before the conferences, I was interviewed by the [Changelog Podcast](https://changelog.com/podcast/346), and spoke about Manyverse, off-grid, and donations models for open source. In other words, that podcast is about this specific Open Collective and future sustainability. Check it out if this topic interests you.

The big event of the month was **[Data Terra Nemo](https://dtn.is/) in Berlin**. People from the SSB community blended with the Dat community and the IPFS one (and others) to share insights on various peer-to-peer interests. In my experience it was fantastic, I met in person all these wonderful people I interact with weekly online. In the picture below you can see me (left), Gordon (middle), and Dominic (right). Gordon helped bring Bluetooth support to Manyverse, and Dominic created the SSB protocol.

![Photograph of Andre (left), Gordon (middle), and Dominic (right), looking towards the camera and smiling; they have long-sleeved shirts and are indoors, in a meetup event, where behind them there are a few people standing having conversations](../../images/201906faces.jpg)

The SSB community was about a dozen or two dozen people. It's hard to say how many are "from SSB" since many seemed to have an SSB account and were not active. Apparently, almost everyone knew about SSB and had a special appreciation for it. Dominic held the opening keynote, and told stories about early Node.js days and how things accidentally (and incidentally) began growing. For more pictures of the event, follow this [thread of tweets](https://twitter.com/andrestaltz/status/1128928703868751872) I posted.

On a Sunday after the conference, the SSB community gathered for what we called "**Scuttle sessions**", where we discussed important social and technical issues we want to do something about. Among other things, we discussed a new binary format for the protocol, browser support and/or Metamask integration, RFC processes and wishes, and one session I called "Swarm collaboration". There are many apps we wish existed, and for vastly different purposes other than just "social networking", but all of them – due to the trustful decentralized nature of SSB – would have a common underlying theme of commons and cooperation. In the picture below, we had a large paper with many post-its attached, one for each session idea or proposal.

![Photograph of a poster with a dozen post-its, placed on a brick floor, outdoors; you can see people's feet around the poster, and one person's finger pointing to the poster, but you cannot see their torso or faces](../../images/201906poster.jpg)

On the funding side of Manyverse, I have lots of great news. But first, one bad news: we (Gordon, Erick Lavoie, and I) didn't pass for the [Ledger](https://www.ledgerproject.eu/) project, not this time. The bummer is that working alongside Gordon and Erick would have been amazing, but on the other hand I got to meet both Gordon and Erick in person this month. The great news about funding is that now this Open Collective has received **8400€ from ACCESS (on behalf of the Scuttlebutt community)**! And just a few weeks ago I got the news that one of the grants I applied for was approved, meaning that **I'll receive 10000€ from [NLnet](https://nlnet.nl/)!** This will happen in milestones: as I complete milestones (directly from our Manyverse feature roadmap), a payment will be sent to my personal bank account. This is outside of Open Collective because it's better for NLnet and for myself to avoid the transaction fees from both Open Collective and PayPal. But for full transparency, I'm reporting the grant here too. In practice, this means that I'm **reducing the full-time goal from 36k€ to 22k€** on this Open Collective. In other words, that goal is now at more than 60% done!

The biggest challenge right now is not funding for myself, but instead increasing the development effort/time put into feature development and bug fixes. I'm looking forward to trying one or two **contracts (gigs)** so that other developers can work on Manyverse too. Such short gigs will be an important step before committing to funding me and another developer full-time. If you want to help develop Manyverse as a paid developer, or if you know someone adequate for this, contact me! [contact@staltz.com](mailto:contact@staltz.com) The requirements are simply to be a skillful developer and quick learner, because the tech stack – being so unique – demands some learning.

In May, besides the bug fixes, I began developing a **new kind of server** (alternative to Pubs) that would fix one of the most important problems with Scuttlebutt: onboarding and finding people. I ran this idea with some people at Data Terra Nemo and there seems to be a consensus that it would work. Currently, I have the server software ready, and working on building the support in the client, in Manyverse. As soon as it becomes usable, I would need some volunteers to try it out. Releasing decentralized software has to be done with extra care because you can't undeploy it once it's out there. If you're interested in testing it and giving feedback, [send me an email](mailto:contact@staltz.com). This will be my only focus for June, and it's also my first milestone for the NLnet funding, so this is very likely to happen!

Thank you for reading, and see you til next newsletter.

Kind regards,

— @andrestaltz