---
path: '/blog/2020-03-update'
date: '2020-03-05'
title: 'Mar 2020 update'
author: 'André Staltz'
authorUrl: 'https://staltz.com'
---

Hey backers, this month we reached a numerical milestone: there are now 100 backers! In February, I worked 72 hours to bring you new features and mark *done* some roadmap tasks. Together, you funded 37 hours of work. There were two releases published, the most significant one was [version 0.2003.4](https://gitlab.com/staltz/manyverse/blob/master/CHANGELOG.md#020034-beta) published yesterday for iOS and Android. The highlights are:

- 🎉 **Feature: settings screen** [v0.2003.4](https://gitlab.com/staltz/manyverse/blob/master/CHANGELOG.md#020034-beta)
- 🎉 **Feature: links to forked threads** [v0.2003.4](https://gitlab.com/staltz/manyverse/blob/master/CHANGELOG.md#020034-beta)
- ☑ **Bug fix: on Android 9+ images were not showing** [v0.2003.4](https://gitlab.com/staltz/manyverse/blob/master/CHANGELOG.md#020034-beta)
- ☑ **Bug fix: connections disappeared, issue [#802](https://gitlab.com/staltz/manyverse/issues/802)** [v0.2002.24](https://gitlab.com/staltz/manyverse/blob/master/CHANGELOG.md#0200224-beta)
- ☑ **Bug fix: crash from issue [#799](https://gitlab.com/staltz/manyverse/issues/799)** [v0.2002.24](https://gitlab.com/staltz/manyverse/blob/master/CHANGELOG.md#0200224-beta)
- ☑ **Bug fix: links in Thanks and About screens** [v0.2002.24](https://gitlab.com/staltz/manyverse/blob/master/CHANGELOG.md#0200224-beta)
- ☑ **Bug fix: simple typo in the welcome screen** [v0.2002.24](https://gitlab.com/staltz/manyverse/blob/master/CHANGELOG.md#0200224-beta)
- 🔷 **Improve autocomplete list when mentioning people** [v0.2002.24](https://gitlab.com/staltz/manyverse/blob/master/CHANGELOG.md#0200224-beta)

As you see, there were quite a few bug fixes. Some bugs were common to many people, and with the right amount of bug reports, we got to the bottom of it. The new version should be more reliable, let us know if there's anything else that obviously needs a fix.

The new **Settings screen** allows you to tweak Manyverse to your liking, mostly regarding data and the usage of storage. My favorite option in the Settings screen is "*Blobs storage limit*": it allows you to set a maximum storage quota for images and other blobs; when that quota is exceeded, Manyverse will automatically begin deleting large and old images. This is brought to you by a new open source library [ssb-blobs-purge](https://github.com/staltz/ssb-blobs-purge).

![A graphic design where there is a phone on the right, displaying the Manyverse Settings screen with a couple of toggles and slider, and text to the left of the phone; The text says "Settings screen, hide follows, free up space, configure hops"](../../images/announcement-settings-screen.png)

Another prominent slider is the "**Replications hops**": this allows you to choose how much data from the social graph should Manyverse download. By default it has been 2, which means it downloads data from your friends, as well as from friends of friends. Some people would prefer to download data only from friends, this is now possible *by settings hops to 1*. As a bonus, this makes initial sync significantly faster, because there is less data to fetch. Hops can also be set to 3 or 4 or unlimited, but you should only do that if you know what you are asking for. ;)

On the "public board" screen, you can often see follow events such as "Alice followed Bob". If you don't like those, you can toggle them off in the Settings screen with the "**Show follow events**" switch.

There are a couple other configurations you can explore: you can access the *Backup* screen, you can *Enable detailed developer logs*, you can *view licenses of third party libraries*, and the *Thanks* and *About* popups can be found too. The Settings screen was also the last milestone we had for [the NLnet grant](https://nlnet.nl/project/Manyverse/). Big **thank you to NLnet** for pushing Manyverse forwards!

The next noticeable feature is **links to forked threads**.

![A graphic design where there is a phone on the right, displaying Manyverse with the Public board screen showing a thread of messages with a note "Forked from...", and text to the left of the phone; The text says "Links to forks, going to the original thread"](../../images/announcement-fork-notes.png)

Manyverse doesn't have a way of explicitly forking threads, but other SSB apps such as Patchwork do. When a thread is forked, it means that one of the replies to that thread becomes a new "thread head" with its own replies. Previously, Manyverse would show such forked threads without any link to the previous thread, and this could be confusing because the thread head may be in reply to something else, but you couldn't see what the upstream conversation was. Now, there is a small cypherlink note on the head of the forked thread, and you can navigate to the previous conversations.

On a final note, contributing code to Manyverse is not the easiest, because the tech stack is so thick and unique. But thanks to [David Gómez](https://gitlab.com/staltz/manyverse/issues/808) who wanted to get started with iOS development, we updated the [Contributing guide](https://gitlab.com/staltz/manyverse/-/blob/master/CONTRIBUTING.md) with more complete instructions. If you have wanted to contribute code too, maybe now it should be easier. I also want to toss in this [tiny tease](https://gitlab.com/staltz/manyverse/-/blob/7c73ddc8d4ce75994b16935b28eaba4fa452e650/package.json#L27).

Thank you! And let's stay in touch. :)

— @andrestaltz