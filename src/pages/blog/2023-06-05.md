---
path: '/blog/2023-06-05'
date: '2023-06-05'
title: 'June 2023 update'
author: 'André Staltz'
authorUrl: 'https://staltz.com'
---

Hey backers,

The new protocol for Manyverse – codename PPPPP — is taking shape quickly, I'm happy to share that we're almost done with the exploratory phase. This time I made progress on the multidevice account system, the server infrastructure, and onboarding flows.

Before I go into details, I want to remind you that we're still doing work on Private Groups. Just recently we completed a few milestones, such as the [user experience design](https://www.manyver.se/blog/2023-05-19). While these groups are designed for SSB, we are taking them into account in the new protocol, and in the following paragraphs I'll explain how.

## Multidevice support

SSB accounts have always been limited to a single device. This is a limitation that we want to overcome in PPPPP. It's not just because people may own multiple devices, it's because solving this problem will automatically give us an authentication system for multiple 3rd party apps. I want PPPPP to securely, easily, and conveniently support an ecosystem of many apps, from day one. I may use the word "device" or "app" interchangeably, because from the protocol's perspective they are equivalent.

A naive approach to this is to just use the same cryptographic keypair on many devices or apps. This is bad for security, because it's enough that just one of these apps is compromised, and the attacker can impersonate you on all other apps. So **no shared keys** is a goal for our account system.

If you never share cryptographic keypairs across devices, then you end up having one public key identifier for each device. This becomes a problem because then there is no simple and unique ID to refer to a person. Hence another goal is to **have a single ID for your cross-device account**.

It is somewhat easy to achieve that by electing one of your keypairs to be the main one, while all others have to ask permission from the main one to join the account. But this creates a hierarchy and a single point of failure. **No hierarchy** is another goal for our account system.

The three goals form a trilemma

- Single ID
- No shared keys
- No hierarchy

And it's surprisingly hard to design for this. But after [thorough ideation and discussion](https://github.com/ssbc/ssb2-discussion-forum/issues/24) we found a way. It requires changes to the feed format, but that's okay at this phase. The trick is to **make your identity a tangle**!

## Identity tangle

If you've been following our work, you know that we're doing everything based on tangles (another word for Directed Acyclic Graphs, DAGs). The feed format is a tangle, and so will your account's identity be.

Your account will be defined by an "identity tangle", where each message declares that a new keypair now belongs to your account. The first message in this tangle is signed by the first keypair you ever had, and **the hash of the first message becomes your unique identifier**. Whenever a new keypair is added to the identity tangle, it has to be signed by one of the keypairs already in the tangle. For instance, if your first device was your desktop, and the second device was your phone, then the message that adds your phone's keypair must be signed by your desktop's keypair.

Now when you publish normal messages, they will contain your identity tangle's ID. Other peers who replicate this message will have to first replicate your identity tangle, and verify that the signature of the message was created using one of the keypairs in your identity tangle.

[There are more details](https://github.com/staltz/ppppp-db/blob/master/protospec.md#msg-v2), but this is the gist of it. I already [implemented this in the database module](https://github.com/staltz/ppppp-db/blob/5c24ffc498769b371eb7588db62b0645a954282c/test/group-add.test.js#L43), and it seems to work!

* ✅ Single ID
* ✅ No shared keys
* ✅ No hierarchy

Apart from supporting multidevice and multi-app accounts, this opens up an exciting possibility of multi-person accounts! Since there is no way you can enforce that all keypairs in your account are physically controlled by the same person, you can add your friend's keypair(s) to an identity tangle, and now you can act as if you're one.

In fact we're talking about groups, and their capability of (1) collaborating on a public document, (2) communicating with the world, and – with the help of encryption – (3) communicating privately with each other. I'm very excited how PPPPP will enable all kinds of "grouping" of people and/or apps, and the design we found is flexible and secure.

## Server infrastructure

I also had time to research and design the network topology for PPPPP. I bumped into a couple of dilemmas in this space too, but eventually found a way forward. The full details are [documented in this GitHub issue](https://github.com/ssbc/ssb2-discussion-forum/issues/25) but I can summarize it as follows.

PPPPP will have servers called **hubs** which are very much like SSB Rooms 2.0, with some minor differences. Hubs will be closed for outsiders, but all members of a hub can freely connect to each other without a (application-level) firewall. There will be no "alias URLs", meaning that you will not be connectable to outsiders via a URL.

The invite system is also very important, because (as hinted in the previous section) inviting a friend to your network is not that different from inviting a new device (or new app) into your account. Identity tangles provide a way of grouping keypairs, but the invite system provides a way of securely *discovering* those keypairs.

Whatever the case is, the flow will always start with your existing device or app creating a link (the "invite") which is passed to another device or person. That link will provide them with the power to (1) join the hub, (2) join your identity tangle or become your friend. The hub plays an essential part in this process, but it doesn't hold authorization power. The authorization power is embedded a token in the invite link, unknown to the hub, and it will be used when the new device directly connects with the old device.

Like in SSB, peer connections will use Secret Handshake for end-to-end encrypted connections. However, for bootstrapping based on these invites, I devised a tweaked variant of Secret Handshake that allows sending the invite token. The token is recognized (or not) during the handshake, and if recognized, the new device is authorized to join the hub and/or identity tangle. I call this **Secret Handshake Extended**.

I'm now implementing the hub as a proof of concept, and I'm hoping that soon I can tie it all together. This will let us see how the invite system works in practice, and how we can create toy apps to demonstrate the multidevice and multi-app account system. Being able to toy around with the new protocol across devices is going to be exciting, and a precursor for the next network-to-be!

The diagram below updates you on the roadmap for PPPPP. The green parts are done, and the yellow parts are in progress. It's clear, we don't have a lot left to research, before we can start implementing it. **Thank you for supporting us!**

![Diagram of tasks laid out as a top-to-bottom graph where parts of the graph are done while other parts are work in progress](../../images/ppppp-roadmap-202306.png)

– @andrestaltz