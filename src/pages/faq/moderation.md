---
date: 'general03'
path: "/faq/moderation"
title: "How does moderation work?"
---

Because the developers of Manyverse have no access or control over the data from all Manyverse users, the moderation in the social network is not the responsibility of the developers. Instead, moderation is a shared responsibility among the communities.

No user can be globally banned from Manyverse, but they can be banned by specific persons. However, unlike mainstream social networks of today, where banning is not so consequential (you can always make a new account or view anyone's profile while logged out), banning on Manyverse is highly consequential because it means the undoing of a network link that binds people together.

For instance, suppose Xavier is talking with Alice, Bob, and Carla (A, B, and C) on Manyverse. If A blocks Xavier, then Xavier can still talk to B and C. But if Xavier is blocked by all three A, B, C, then Xavier can't even get a network connection with any of them, and therefore cannot get their data. Xavier is effectively banned from the social circle A, B, C, even though he is free to communicate with others. The only way how Xavier can reconnect with them is if he is *re-invited* by one of them.

This puts special importance on *blocks*. In Manyverse, you can **block accounts either publicly or secretly**. Use public blocking to block plus **signal to your friends** that they should also consider blocking that account. Use secret blocking as a way of privately choosing to not connect to that account, also known as "mute" on other social networks.

Currently, blocking is manual and propagates only through social conventions in communities. Later, we will build better moderation tools in Manyverse so you could assign a trusted friend to do blocks on your behalf, effectively electing them as a moderator of your social circles. You could have multiple opt-in trusted moderators, and other people could also choose you as a moderator. The role of moderation would be informal and ephemeral, not official and binding.
