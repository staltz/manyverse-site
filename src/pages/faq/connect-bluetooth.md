---
date: 'connections02'
path: "/faq/connect-bluetooth"
title: "How do I use Bluetooth to connect with people?"
---

**⚠️ Bluetooth connections are no longer supported in Manyverse for four reasons:**

1. In late 2022, Google released a new version of Android 12+ that intentionally crashed any app using Bluetooth capabilities (such as Manyverse) that doesn't request Bluetooth permission as soon as it is attempted. That was the case for Manyverse, and this was working on all previous version of Android. It was Google who was doing this to users, not us. They could have chosen to display a warning popup, or something else, but instead they chose the drastic option of killing the app and blaming the app devs.
2. Developing the new permission runtimes requires extra testing to make sure it works and as developers we don't have Android 12+ devices lying around to test this, and testing Bluetooth on Browserstack is kind of pointless since we don't have "devices nearby" in the cloud.
3. We plan to phase out Bluetooth connections in Manyverse in the future when we build Wi-Fi P2P sync (see ticket [#1591](https://gitlab.com/staltz/manyverse/-/issues/1591)), and because Bluetooth connections are complex and currently not implemented on iOS neither Desktop.
4. Probably not a lot of people depend on this feature, and if they do, they have the alternative of starting a mobile hotspot and doing Wi-Fi sync in Manyverse instead. Wi-Fi sync in this style is even easier and more efficient than Bluetooth sync.

This is a shame, because this was a feature we were proud of. But the reality was that Google forced our hand: it is more important to make the app not crash for users on Android 12+. We are really looking forward to building a comprehensive Wi-Fi sync system that will serve the off-grid use cases well.

-----

Bluetooth connections work only with other phones nearby, usually a few meters close. The farther away the phones are, the more difficult the connections are established. Also, the more other Bluetooth devices there are around (such as loud speaker or mouse or headphones), the harder it is to connect in Manyverse, so make sure you find a good isolated corner. So far, Bluetooth connections only work between Manyverse and Manyverse, not with other apps.

Once you find a friend whom you wish to connect with over Bluetooth, **both of you should follow the instructions below**, and preferably with the phones less than 1 meter apart.

First make sure your phone supports Bluetooth. This is true for most devices, but to be sure, open the *Settings* on your Android phone, and look for the menu *Connections*, and then *Bluetooth*. **Turn on Bluetooth in the Android settings.**

Then, **open Manyverse** and then once you are on the screen that says *Messages*, go to the tab on the right with a globe icon, to see the *Connections* tab. The Bluetooth icon should be highlighted blue, meaning it's enabled. If it's not blue yet, wait a few seconds.

On this *Connections* tab, there is a **green circular button** at the bottom, tap that to open a small menu, and then choose **Bluetooth seek**. The other phone should do this too, at the same time.

A dialog will open asking for your permission to scan Bluetooth, this is normal and you should give it permission. Then, **wait for your friend's phone to appear** in the *Connections* tab. The Bluetooth icon in this screen will show a loading spinner indicator whenever it is scanning for nearby friends. If this spinner does not show anymore, then repeat the operation of pressing **Bluetooth seek** in the green button's menu. Remember, your friend needs to follow these same instructions at the same time as you.

Once your friend appears in the *Connections* tab, their name might show as the *name of the device* (e.g. Samsung Galaxy) not the name of their profile in Manyverse. Make sure that device name is actually from your friend's device. Then, just **tap** your friend's list item to connect with them.

- Choose **Connect and follow** to create a Bluetooth link AND follow their Manyverse profile
  - This is useful to fetch their data, you should choose this option when the other person is your friend
- Choose **Connect** to ONLY create a Bluetooth link without following them
  - This is useful when the other person is an acquaintance and you only want to fetch updates from their social circles but not from this person
