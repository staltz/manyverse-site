---
date: 'account02'
path: "/faq/desktop-migration"
title: "Can I use my Patchwork feed in Manyverse?"
---

**Yes, but only if you migrate** from `~/.ssb` to Manyverse's own folders.

Typically, SSB apps on desktop such as Patchwork, Patchbay, and Oasis all share the same `~/.ssb` folder, and thus you can use the same account on all of them. **Manyverse cannot run on the database that is at `~/.ssb`**, because it uses a new database that is incompatible with the other apps.


When starting up Manyverse, you will be asked to make a choice between:

**Creating a new account.** We recommend this if you may want to keep using Patchwork as usual.

**Migrating from `~/.ssb`.** Not recommended, but it works correctly and you can transfer your existing SSB content to Manyverse. It's important to know **there is no going back**, unless you open Patchwork to create a fresh account.

[Read here](https://manyver.se/faq/each-app-own-folder) about why the migration is necessary.
