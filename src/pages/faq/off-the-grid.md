---
path: "/faq/off-the-grid"
date: 'general02'
title: "How is this off the grid?"
---

Most social network apps today are internet-first: if you have no internet connectivity, then you basically have no access to your social network, not even your own posts.

**Manyverse is [local-first](https://www.inkandswitch.com/local-first.html).** It always works, even without internet connectivity. When you post something, it is *first* saved locally on your phone. It only gets uploaded to the internet once your phone has internet connectivity. But even if you remain always offline, you could still share your data by uploading to nearby phones via Wi-Fi. So we say that Manyverse is "off the grid" because it can function fully even if you never connect to the "grid", in other words, to an internet provider and to the rest of the world.

While most people using social networks today are on the internet, there is a vast amount of people in Africa, Latin America, and Asia, who have precarious or no internet access. These people also want to communicate with their communities in a modern fashion, and local connectivity modes such as Wi-Fi could be used. Manyverse exists to fill that gap. And it's not exclusively for off grid, it of course also works over the internet. Either if there is internet connectivity or not, Manyverse can be used and useful for social networking.

This aspect is also useful for those people who suddenly lose internet connectivity, either because of some natural disaster affecting the grid, or the censorship of the internet by an authoritarian government, or other reasons. If word of mouth and paper can be used in a variety of circumstances as communication means, then so should mobile social networking be always available. Manyverse exists to fulfil that need.
