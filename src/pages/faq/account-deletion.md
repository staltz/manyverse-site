---
date: 'account05'
path: "/faq/account-deletion"
title: "How can I delete my account?"
---

Your account is just a folder on your device. To delete it, you can delete that
folder. Read here about the [location of the Manyverse folder](/faq/manyverse-folder-location).

For convenience, you can also delete that data from within the app. On the left-side menu, go to the Settings screen, scroll to the bottom, then select "Delete account".

Note that we don't have a "backend" where accounts are managed and granted access to. Manyverse developers do not provide an internet "service", we just develop and publish software. We are not a company, and do not host user data in any shape.

If you want to delete all your data from the SSB network, this requires reaching out to your contacts and asking them to "block" you on their Manyverse app. This will cause their app to delete all your data from their device. As a decentralized network, this is a similar situation to when you share your pictures with friends over email or AirDrop or USB drives: you need to ask each friend to delete the pictures from their device. There is no global "delete" button for this.
