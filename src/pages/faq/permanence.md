---
date: 'general04'
path: "/faq/permanence"
title: "How do I delete content?"
---

The short answer is that it's not possible to delete your posts or other data from Manyverse.

This is due to design choices in the [SSB protocol](https://scuttlebutt.nz) and because of how data spreads in an off grid social network.

It's hard to remove your data from the internet, no matter what the platform is, because people can keep copies of it. For instance, it's common to keep screenshots of posts on Twitter or Facebook, that prevents the author from retracting what they said even if they delete their post. However, deletion on those centralized platforms is still useful, and helps the author gain confidence that others don't have copies of their data.

On SSB, deletion is more complicated because the social network is *by design* a network of data copies. When you view your friends' posts, you are actually viewing a *copy* of their posts. So if your friend were to delete their post, in practice they would have to issue a request for everyone else to also delete copies of that post. These deletion requests could be built in an automated way — and indeed the SSB protocol is being redesigned to allow deletes in the future — but the propagation of requests is made more difficult in an truly off grid scenario.

For instance, suppose Alice is friends with Bob, and Bob posts something new. Alice receives a copy of that post, reads it, and keeps it. *Then*, Alice leaves to spend months in Africa off grid (without internet), while Bob remains in the city with internet. As soon as Alice arrives in Africa, Bob suddenly regrets posting his latest message, so he wishes to delete it. However, he has no way to reach Alice and inform her to delete her copy of the post. Alice, in Africa, further propagates Bob's post to a large community of people using local synchronization, without internet, and Alice is oblivious to the fact that Bob does not want that post shared. There is no way Alice can know and respect Bob's wishes for deletion, and the message can keep propagating for months.

While we cannot truly support instant global deletion in the way users from centralized media expect, there are several things we can do to improve this situation. Some technical solutions include implementing propagated-deletes with [a new feed format](https://github.com/ssbc/ssb-buttwoo-spec/) that supports deletes, and implementing deletion-requests for blobs (pictures and attachments) that you have published.

As non-technical solutions, we want to communicate more clearly. We warn users to enter the SSB space with a keen understanding of the lack of deletes, and acknowledge the risk of your content permanently remaining in the network. Some users should refrain from using Manyverse and SSB to begin with, such as people protesting against authoritarian regimes, because they could use SSB personal data as evidence. Do not use SSB for critically political discourse and organizing.

We want to make the world a better place, and for that we need to be sure that our design choices are actually promoting good effects. We're still learning those effects and adjusting our choices. In the meantime, consider the SSB network an experiment with potential side effects.
