---
date: 'account03'
path: "/faq/each-app-own-folder"
title: "Why can't Manyverse use the .ssb folder?"
---

Manyverse developers believe that **each app should have its own folder** and account, even if they are all running on the same device. There are several reasons why `~/.ssb` shared among all apps is a bad idea, such as:

### Sharing the folder causes bugs

When apps share the same folder and database, they can easily mess up each other's functionality. For instance, update Patchwork and it may rebuild one specific database index which Oasis was using, and then Oasis is broken. Or vice-versa. Often people have avoided those headaches by using only one app regularly. Or they have to delete all indexes before taking the other app into use, to force indexes to be rebuilt.

Other solutions like a "hub app" or Scuttleshell share similar kinds of consensus-based headaches.

### Flume versus ssb-db2

One hard reason that forces us to do something different in Manyverse is that Patchwork writes to `flume/log.offset` while Manyverse uses ssb-db2 which is incompatible with flume and writes to `db2/log.bipf`. These two logs cannot be written at the same time, otherwise you risk forking your feed. There can only be one source of truth, either one of those logs has to be the main one. Also, streaming messages from one to the other is worse for performance.

### Easier to develop an app

Beyond just Manyverse, we want to see an ecosystem of hundreds of SSB apps, and it's easy to create an app when the app has 100% control over its own database. Optimizing for developer productivity and simplicity is a good bet for the ecosystem of SSB apps.

### Co-locating with off-log databases

Say you need to store some data outside of the SSB log, like \"unread state\" or simple things like your choice of color theme in the app. Often these are stored in things like (or resembling) `localStorage`, and that's already true for Patchwork, it has *some* app data stored in `~/.config/Patchwork`, **not** `~/.ssb/Patchwork`. So it would be convenient that if you delete `~/.config/Patchwork` (say, during uninstalling) it would also delete the associated SSB data for Patchwork. If we colocate the SSB log with the SSB app data, this is simple. And also, it's more easily backed up or transferred from one computer to another, because it's just one folder you need to worry about.

### Some operating systems demand it

You can't have an equivalent to `~/.ssb` on iOS (or at least it's very difficult), and Android just recently introduced a similar constraint, and this left me scratching my head for months, how to work around it.

Because we follow the one-folder-per-app mantra, it is supported on **all** operating systems, so it's a good and simple strategy regardless what platform we're developing for.

### Metafeeds and fusion identity will help us

These new subprotocols will enable us to properly tie together data from different apps. The solution for \"I want my account on all apps\" isn't to have literally the same feed ID on all of them, but instead something more flexible like same-as (Fusion Identity) and subfeeds under a root metafeed. This way, each app can choose its own feed format (this is good for the future if we want to update feed formats), without messing with other apps.

### Security

With the current `~/.ssb` state of affairs, it's enough that one rogue app has access to it (say, you try out a proprietary SSB app just once), and it could upload your secret to a server. Then you're fully compromised. Such app is actually very easy to build.

With one app per folder (and especially with some operating systems like iOS and package managers such as snap), it may be hard or impossible for other apps to sniff into one app's folder.
