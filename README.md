# Manyver.se website

If you want to contribute changes to the website, first of all, thank you!

- Note our [Code of conduct](https://gitlab.com/staltz/manyverse/blob/master/code-of-conduct.md)
- If you want to **improve** the site, first discuss it as an issue before opening any merge request
- If you want to **fix** a small issue, any merge request is accepted, with prior issue or not

The website is built using [Gatsby](https://www.gatsbyjs.org) as framework.

## Development

We haven't had the chance to update Gatsby yet, so for this project **you need to use Node.js version 10**.

Then

```
npm install
```

You will need a GitLab Access Token which you'll have to put under the environment variable `GITLAB_ACCESS_TOKEN`.

Then run

```
npm run develop
```

## Deploying

```
npm run build
```

then

```
npm run deploy
```
